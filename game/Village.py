import Files

# For containing structure's lore
class StructureLore:
    type = 0
    lvl = 0
    name = ""
    description = ""

    def __init__(self, type, lvl, name, description):
        self.type = type
        self.lvl = lvl
        self.name = name
        self.description = description
        
    def __str__(self):
        return "Name: " + self.name + "\nDescription: " + self.description

    def structInfo(self):
        return "Name: " + self.name + ", Type: " + str(self.type) + ", Level: " + str(self.lvl)

# Basic structure class
class Structure:
    type = 0
    lvl = 1

    def __init__(self):
        self.lvl = 1
    
    def upgrade_lvl(self):
        if self.lvl < 10:
            self.lvl += 1
            return True
        else:
            
            return False

    def toString(self):
        return getStructureName(self.type, self.lvl) + ", level = " + str(self.lvl)

    def printLore(self, loreData):
        print(self)
        print(loreData[self.type][self.lvl-1])

    def structInfo(self):
        return "Name: " + self.name + ", Type: " + str(self.type) + ", Level: " + str(self.lvl)

    def structUserInfo(self):
        return "Name: " + self.name + ", Level: " + str(self.lvl)
    
    def getLoreName(self):
        return "Name: "

class coinProducer(Structure):
    def __init__(self):
        super().__init__()
        self.type = 1
    
    def setLvl(self, lvl):
        self.lvl = lvl

    def __str__(self):
        return self.toString() + ", type[1]: coin generator"

class shelter(Structure):
    def __init__(self):
        super().__init__()
        self.type = 2
    
    def setLvl(self, lvl):
        self.lvl = lvl

    def __str__(self):
        return self.toString() + ", type[2]: shelter"
 
class foodProducer(Structure):
    def __init__(self):
        super().__init__()
        self.type = 3
    
    def setLvl(self, lvl):
        self.lvl = lvl

    def __str__(self):
        return self.toString() + ", type[3]: food producer"

class Village:
    bot = False
    lvl = 0
    coins = 0
    coinProd = 0
    coinCounter = 0
    structs = []

    coinProducerNum = 0
    shelterNum = 0
    foodProducerNum = 0

    # Generate lvl 1 village with 1 of each structures with level 1
    def __init__(self):
        self.bot = False
        self.lvl = -2
        self.coins = 0
        self.coinProd = 0
        self.coinCounter = 0
        self.structs = []

        self.coinProducerNum = 0
        self.shelterNum = 0
        self.foodProducerNum = 0

        self.addStructure(1)
        self.addStructure(2)
        self.addStructure(3)
    
    def __str__(self):
        r = "Level: " + str(self.lvl) + "\n"
        r += "Coins: " + str(self.coins) + "\n"
        r += "Coin production: " + str(self.coinProd) + "\n"
        r += "Structures:\n"
        for s in self.structs:
            r += str(s) + "\n" 
        r = r[:-1]
        return r
    
    def reducedPrint(self):
        print("Level: " + str(self.lvl))
        print("Coins: " + str(self.coins))
   
   # Set all values
    def setAll(self, lvl, coins, coinProd, structs, coinProducerNum, shelterNum, foodProducerNum):
        self.lvl = lvl
        self.coins = coins
        self.coinProd = coinProd
        self.structs = structs
        self.coinProducerNum = coinProducerNum
        self.shelterNum = shelterNum
        self.foodProducerNum = foodProducerNum
        self.coinCounter = 0

    # Add a structure according to the type and adding to the count
    def addStructure(self, type):
        if len(self.structs) < 99:
            if type == 1:
                newStructure = coinProducer()
                self.coinProducerNum += 1
                self.coinProd += 1
                self.structs.append(newStructure)
            elif type == 2:
                newStructure = shelter()
                self.shelterNum += 1
                self.structs.append(newStructure)
            elif type == 3:
                newStructure = foodProducer()
                self.foodProducerNum += 1
                self.structs.append(newStructure)
            self.lvl += 1
            return True
        else:
            return False
    
    # Delete structure from structure list
    def delStructure(self, type, lvl):
        for i in range(len(self.structs)):
            if self.structs[i].type == type and self.structs[i].lvl == lvl:
                # If mine, reduce coin production
                if self.structs[i].type == 1:
                    self.coinProd -= self.structs[i].lvl
                    self.coinProducerNum -= 1
                elif self.structs[i].type == 2:
                    self.shelterNum -= 1
                elif self.structs[i].type == 3:
                    self.foodProducerNum -= 1

                # Reduce level to the village
                self.lvl -= self.structs[i].lvl

                # Remove structure from array moving the last
                if i != len(self.structs) - 1:
                    self.structs[i] = self.structs[len(self.structs) - 1]
                self.structs.pop()

                return True
        return False
    
    # Upgrade structure
    def upgradeStructure(self, type, level):
        struct = self.searchStructure(type, level)
        # If structure is found
        if struct != None:
            # Upgrade structure level
            struct.upgrade_lvl()
            # Upgrade village level
            self.lvl += 1
            # If coind producer, add to the village production
            if struct.type == 1:
                self.coinProd += 1
            return True
        else:
            return False

    # Searching structure
    def searchStructure(self, type, level):
        for i in range(len(self.structs)):
            if self.structs[i].type == type and self.structs[i].lvl == level:
                # Return if structure found
                return self.structs[i]
        # Return None if not found
        return None
    
    # Produce coins interaction
    def produceCoin(self):
        self.coinCounter += 1
        if self.coinCounter > 5:
            self.coins += self.coinProd
            self.coinCounter = 0

def getStructureName(type, lvl):
    lore = Files.readAllStructures()
    for i in range(len(lore)):
        if lore[i].type == type and lore[i].lvl == lvl:
            return lore[i].name
    return "Not found"

if __name__ == "__main__":
    print(getStructureName(3, 10))