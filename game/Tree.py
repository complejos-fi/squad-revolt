class BtreeNode:
    def __init__(self, t , leaf):
        self.t = t
        self.usernames = [None] * (2 * t - 1)
        self.C = [None] * (2 * t)
        self.n = 0
        self.leaf = leaf
        
    def insertNonFull(self,user):
        i = self.n - 1
        if self.leaf:
            while i >= 0 and self.usernames[i].username > user.username:
                self.usernames[i + 1] = self.usernames[i]
                i = i - 1
            self.usernames[i + 1] = user
            self.n = self.n + 1
        else:
            while i >= 0 and self.usernames[i].username > user.username:
                i = i - 1
            if self.C[i + 1].n == 2 * self.t - 1:
                self.splitChild(i + 1, self.C[i + 1])
                if self.usernames[i + 1].username < user.username:
                    i = i + 1
            self.C[i + 1].insertNonFull(user)
    
    def splitChild(self, i, y):
        z = BtreeNode(y.t, y.leaf)

        z.n = self.t - 1
        j = 0
        while j < self.t - 1:
            z.usernames[j] = y.usernames[j + self.t]
            j = j + 1
        if not y.leaf:
            j = 0
            while j < self.t:
                z.C[j] = y.C[j + self.t]
                j = j + 1
        y.n = self.t - 1
        j = self.n
        while j > i:
            self.C[j + 1] = self.C[j]
            j = j - 1
        self.C[i + 1] = z
        j = self.n - 1
        while j >= i:
            self.usernames[j + 1] = self.usernames[j]
            j = j - 1
        self.usernames[i] = y.usernames[self.t - 1]
        self.n = self.n + 1
 
    def traverse(self, l):
        i = 0
        while i < self.n:
            if not self.leaf:
                self.C[i].traverse(l + 1)
            print("\t" * l, l, self.usernames[i], end='')
            i = i + 1
        print()
        if not self.leaf:
            self.C[i].traverse(l + 1)

    def toList(self, l):
        i = 0
        while i < self.n:
            if not self.leaf:
                self.C[i].toList(l)
            l.append([self.usernames[i].username , self.usernames[i].passwd])
            i = i + 1
        if not self.leaf:
            self.C[i].toList(l)
        
    def search(self, k):
        i = 0
        while i < self.n and k > self.usernames[i].username:
            i = i + 1
        if i < self.n and k == self.usernames[i].username:
            return self.usernames[i]
        if self.leaf:
            return None
        return self.C[i].search(k)
        
    def softDelete(self, k):
        i = 0
        while i < self.n and k > self.usernames[i].username:
            i = i + 1
        if i < self.n and k == self.usernames[i].username:
            self.usernames[i].username = ""
            self.usernames[i].passwd = ""
            return True
        if self.leaf:
            return False
        return self.C[i].softDelete(k)



class BTree:
    def __init__(self, t):
        self.root = None
        self.t = t
            
    def traverse(self):
        if self.root != None:
            self.root.traverse(0)
    
    def toList(self):
        l = []
        if self.root != None:
            self.root.toList(l)
        return l

    def search(self,k):
        return None if self.root == None else self.root.search(k)

    def delete(self,k):
        return False if self.root == None else self.root.softDelete(k)
        
    def insert(self, user):
        if self.root == None:
            self.root = BtreeNode(self.t,True)
            self.root.usernames[0] = user
            self.root.n = 1
        else:
            if self.root.n == 2 * self.t - 1:
                s = BtreeNode(self.t, False)
                s.C[0] = self.root
                s.splitChild(0,self.root)
                i = 0
                if s.usernames[0].username < user.username:
                    i = i + 1
                s.C[i].insertNonFull(user)
                self.root = s
            else:
                self.root.insertNonFull(user)