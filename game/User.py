import random
import string
import hashlib
import re
import string
import Village
import Tree
import Files

class User:
    username = ""
    passwd = ""
    village = None

    def __init__(self, username, passwd):
        self.username = username
        self.passwd = passwd
        self.village = None
    
    def setVillage(self, village):
        self.village = village
    
    def __str__(self):
        return "Username: " + self.username

def botGeneration(username):
    t = Files.readUsers()

    # User generation
    bot = User(username, "-1")
    t.insert(bot)
    Files.writeUsers(t)

    # Village generation
    Files.createUserDirectory(username)
    v = Village.Village()
    v.bot = True

    # Random village generation
    lvl = 0
    coins = random.randint(15, 1000)
    coinProd = 0
    structs = []
    cpn = 0
    sn = 0
    fpn = 0
    for _ in range(random.randint(3,90)):
        c = random.randint(1,3)
        l = random.randint(1,10)
        if c == 1:
            t = Village.coinProducer()
            cpn += 1
            coinProd += l
        elif c == 2:
            t = Village.shelter()
            sn += 1
        elif c == 3:
            t = Village.foodProducer()
            fpn += 1
        t.setLvl(l)
        lvl += l
        structs.append(t)
    v.setAll(lvl, coins, coinProd, structs, cpn, sn, fpn)

    # Save village
    Files.writeVillage(username, v)

def md5Hash(s):
    md5_hash = hashlib.md5()
    md5_hash.update(s.encode('utf-8'))
    return md5_hash.hexdigest()

def userGeneration(username, password):
    return User(username, md5Hash(password))

def userLoginIT(tree):
    userName = input("Type user name: ")
    searched = tree.search(userName)
    if (searched == None):
        print("Non existant user")
        return None

    for _ in range(3):
        pswd = input("Type you password: ").lstrip()
        pswd = md5Hash(pswd)
        if pswd == searched.passwd:
            return searched
        else:
            print("Incorrect pasword")

    return None

def registerUserIT(tree):
    userName = input("Type user name: ")
    searched = tree.search(userName)
    while searched != None or userName == "" or not re.match("^[a-zA-Z0-9]*$", userName):
        if searched != None:
            print("User name already registered")
        elif userName == "":
            print("Not a valid username")
        else:
            print("Only alfanumeric characters permited")
        userName = input("Type another user name: ")
        searched = tree.search(userName)
    pswd = input("New password: ")
    pswd = md5Hash(pswd)

    newUser = User(userName, pswd)
    tree.insert(newUser)
    Files.writeUsers(tree)
    Files.createUserDirectory(userName)
    v = Village.Village()
    Files.writeVillage(userName, v)
    return newUser

if __name__ == "__main__":
    print("hola")
    #tree = Files.readUsers()
    #registerUserIT(tree)
    #print(userLoginIT(tree))
    #for i in range(1000):
        #botGeneration("bot_"+str(i))
