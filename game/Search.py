import Files
import random

def linearSearchName(l, r):
    res = []
    n = len(l)
    r = r.lower().strip()

    for i in range(n):
        if r in l[i].name.lower():
            res.append(l[i])
    
    return res

def linearSearchLevel(l, r):
    res = []
    n = len(l)

    for i in range(n):
        if l[i].lvl == r:
            res.append(l[i])
    
    return res

def linearSearchType(l, r):
    res = []
    n = len(l)

    for i in range(n):
        if l[i].type == r:
            res.append(l[i])
    
    return res

if __name__ == "__main__":
    data = Files.readAllStructures()
    structList = []
    n = 100

    for _ in range(n):
        structList.append(data[random.randint(0, len(data) - 1)])
    
    for i in range(n):
        print(structList[i].structInfo())

    searchedList = []
    strToFind = data[random.randint(0, len(data) - 1)].name
    print()
    print(strToFind)
    print()
    searchedList = linearSearchName(structList, strToFind)

    for s in searchedList:
        print(s.structInfo())
    
    searchedList = []
    levelToFind = data[random.randint(0, len(data) - 1)].lvl
    print()
    print(levelToFind)
    print()
    searchedList = linearSearchLevel(structList, levelToFind)

    for s in searchedList:
        print(s.structInfo())
        
    searchedList = []
    typeToFind = data[random.randint(0, len(data) - 1)].type
    print()
    print(typeToFind)
    print()
    searchedList = linearSearchType(structList, typeToFind)

    for s in searchedList:
        print(s.structInfo())