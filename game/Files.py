import csv
import shutil
from collections import defaultdict
import Village
import Tree
import Graph
import User

LORE_DIR = "lore/"
DATA_DIR = "../data/"

def readStructureLore():
    data = defaultdict(list)
    fileName = LORE_DIR + "structures.csv"

    with open(fileName, mode='r') as file:
        csvFile = csv.reader(file)
        next(csvFile)
        for line in csvFile:
            data[int(line[0])].append(Village.StructureLore(int(line[0]), int(line[1]), line[2], line[3]))
    return data

def lore2User():
    res = ""
    data = []
    fileName = LORE_DIR + "structures.csv"

    with open(fileName, mode='r') as file:
        csvFile = csv.reader(file)
        next(csvFile)
        for line in csvFile:
            data.append([line[2], line[3]])

    for i in range(len(data)):
        piv = []
        piv = data[i]
        res +="Structure Name: " + piv[0] + ", Description: " + piv[1] + "\n"

    return res

def readAllStructures():
    data = []
    fileName = LORE_DIR + "structures.csv"

    with open(fileName, mode='r') as file:
        csvFile = csv.reader(file)
        next(csvFile)
        for line in csvFile:
            data.append(Village.StructureLore(int(line[0]), int(line[1]), line[2], line[3]))
    return data

def readUsers():
    tree = Tree.BTree(3)
    fileName = DATA_DIR + "users.csv"

    with open(fileName, mode='r') as file:
        csvFile = csv.reader(file)
        next(csvFile)
        for line in csvFile:
            username = line[0]
            password = line[1]
            user = User.User(username, password)
            tree.insert(user)
    return tree

def readUsernamesList():
    fileName = DATA_DIR + "users.csv"
    data = []

    cnt = 0
    with open(fileName, mode='r') as file:
        csvFile = csv.reader(file)
        next(csvFile)
        for line in csvFile:
            data.append([line[0], int(cnt)])
            cnt += 1

    return data

def readEdges():
    fileName = DATA_DIR + "relationGraph.csv"
    l = []

    with open(fileName, mode='r') as file:
        csvFile = csv.reader(file)
        next(csvFile)
        for line in csvFile:
            l.append(Graph.Edge(int(line[0]), int(line[1])))
    
    return l

def writeEdges(edges):
    fileName = DATA_DIR + "relationGraph.csv"

    with open(fileName, "w", newline="") as csvfile:
        writer = csv.writer(csvfile)
        writer.writerow(["userU", "userV"])
        for e in edges:
            writer.writerow(e)

def writeUsers(tree):
    fileName = DATA_DIR + "users.csv"

    data = tree.toList()
    with open(fileName, "w", newline="") as csvfile:
        writer = csv.writer(csvfile)
        writer.writerow(["username", "password"])
        for row in data:
            if row[0] != "" and row[1] != "":
                writer.writerow(row)

def readVillage(username):
    villageFile = DATA_DIR + "usersData/" + username + "/village.csv"
    structFile = DATA_DIR + "usersData/" + username + "/structures.csv"
    structs = []

    with open(structFile, mode='r') as file:
        csvFile = csv.reader(file)
        next(csvFile)
        for line in csvFile:
            type = int(line[0])
            if type == 1:
                newStructure = Village.coinProducer()
                newStructure.setLvl(int(line[1]))
            elif type == 2:
                newStructure = Village.shelter()
                newStructure.setLvl(int(line[1]))
            elif type == 3:
                newStructure = Village.foodProducer()
                newStructure.setLvl(int(line[1]))
            structs.append(newStructure)

    v = Village.Village()

    level = 0
    coins = 0
    coinProduction = 0
    coinProducerNum = 0
    shelterNum = 0
    foodProducerNum = 0

    with open(villageFile, mode='r') as file:
        csvFile = csv.reader(file)
        next(csvFile)
        for line in csvFile:
            level = int(line[0])
            coins = int(line[1])
            coinProduction = int(line[2])
            coinProducerNum = int(line[3])
            shelterNum = int(line[4])
            foodProducerNum = int(line[5])

    v.setAll(level, coins, coinProduction, structs, coinProducerNum, shelterNum, foodProducerNum)

    return v

def writeVillage(username, village):
    villageFile = DATA_DIR + "usersData/" + username + "/village.csv"
    structFile = DATA_DIR + "usersData/" + username + "/structures.csv"

    # Write structures
    with open(structFile, "w", newline="") as csvfile:
        writer = csv.writer(csvfile)
        writer.writerow(["type", "level"])
        for struct in village.structs:
            writer.writerow([struct.type, struct.lvl])
    
    # Write village
    with open(villageFile, "w", newline="") as csvfile:
        writer = csv.writer(csvfile)
        writer.writerow(["level", "coins", "coinProduction", "coinProducerNum", "shelterNum", "foodProducerNum"])
        writer.writerow([village.lvl, village.coins, village.coinProd, village.coinProducerNum, village.shelterNum, village.foodProducerNum])


def readFightHistory(username):
    fileName = DATA_DIR + "usersData/" + username + "/fightHistory.csv"
    data = []

    with open(fileName, mode='r') as file:
        csvFile = csv.reader(file)
        next(csvFile)
        for line in csvFile:
            data.append([line[0], line[1], int(line[2])])
    return data

def writeFightHistory(usernameAttacked, usernameDefended, gold):
    fileNameAttacked = DATA_DIR + "usersData/" + usernameAttacked + "/fightHistory.csv"
    fileNameDefended = DATA_DIR + "usersData/" + usernameDefended + "/fightHistory.csv"

    with open(fileNameAttacked, "a", newline="") as csvfile:
        writer = csv.writer(csvfile)
        writer.writerow([usernameAttacked, usernameDefended, gold])
    with open(fileNameDefended, "a", newline="") as csvfile:
        writer = csv.writer(csvfile)
        writer.writerow([usernameAttacked, usernameDefended, gold])


def createUserDirectory(username):
    exampleDir = "../data/usersData/exampleUser"
    newDir = "../data/usersData/" + username
    shutil.copytree(exampleDir, newDir)

if __name__ == "__main__":
    data = readStructureLore()
    #for key, value in data.items():
        #print("Key: " + str(key))
        #print("Data: ")
        #for s in value:
            #print(s)
    
    print(data[3][9])

    tree = readUsers()
    #tree.traverse()
    #writeUsers(tree)

    #createUserDirectory("padrino")
    v = readVillage("exampleUser")
    print(v)
    writeVillage("exampleUser", v)