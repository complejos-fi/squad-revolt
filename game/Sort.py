import Village
import Files
import random

def padrinoSortName(collection):
    n = len(collection)
    sortedList = []
    sortedListSize = 0
    element = 0

    if n == 0:
        return []
    
    sortedList.append(collection[0])

    mid = 0
    left = 0
    right = 0
    pos = -1

    i = 1
    while i < n:
        element = collection[i].name
        toInsert = collection[i]
        sortedListSize = len(sortedList)

        pos = -1
        mid = 0
        left = 0
        right = sortedListSize - 1

        while left < right:
            if pos != -1:
                break
            
            if (right-1) == left:
                if element < sortedList[left].name and element < sortedList[right].name:
                    pos = left
                    continue
                elif element > sortedList[left].name and element > sortedList[right].name:
                    pos = right + 1
                    continue
                else:
                    pos = right
                    continue
            else:
                mid = (left + right) // 2

            if element == sortedList[mid].name:
                pos = mid
                continue
            elif element < sortedList[mid].name:
                right = mid - 1
            else:
                left = mid + 1
        
        if pos == -1:
            if element < sortedList[left].name:
                pos = left
            else:
                pos = left + 1
        
        sortedList.insert(pos, toInsert)
        i = i + 1
        
    return sortedList

def padrinoSortUserName(collection):
    n = len(collection)
    sortedList = []
    sortedListSize = 0
    element = 0

    if n == 0:
        return []
    
    sortedList.append(collection[0])

    mid = 0
    left = 0
    right = 0
    pos = -1

    i = 1
    while i < n:
        element = collection[i]
        toInsert = collection[i]
        sortedListSize = len(sortedList)

        pos = -1
        mid = 0
        left = 0
        right = sortedListSize - 1

        while left < right:
            if pos != -1:
                break
            
            if (right-1) == left:
                if element < sortedList[left] and element < sortedList[right]:
                    pos = left
                    continue
                elif element > sortedList[left] and element > sortedList[right]:
                    pos = right + 1
                    continue
                else:
                    pos = right
                    continue
            else:
                mid = (left + right) // 2

            if element == sortedList[mid]:
                pos = mid
                continue
            elif element < sortedList[mid]:
                right = mid - 1
            else:
                left = mid + 1
        
        if pos == -1:
            if element < sortedList[left]:
                pos = left
            else:
                pos = left + 1
        
        sortedList.insert(pos, toInsert)
        i = i + 1
        
    return sortedList

def padrinoSortShow(collection):
    n = len(collection)
    sortedList = []

    if n == 0:
        return []

    sortedList.append(collection[0])

    i = 1
    while i < n:
        element = Village.getStructureName(collection[i].type, collection[i].lvl)
        toInsert = collection[i]
        sortedListSize = len(sortedList)

        pos = -1
        left = 0
        right = sortedListSize - 1

        while left <= right:
            mid = (left + right) // 2

            if element == Village.getStructureName(sortedList[mid].type, sortedList[mid].lvl):
                pos = mid
                break
            elif element < Village.getStructureName(sortedList[mid].type, sortedList[mid].lvl):
                right = mid - 1
            else:
                left = mid + 1

        if pos == -1:
            pos = left

        sortedList.insert(pos, toInsert)
        i += 1

    return sortedList

def padrinoSortLevel(collection):
    n = len(collection)
    sortedList = []
    sortedListSize = 0
    element = 0

    if n == 0:
        return []
    
    sortedList.append(collection[0])

    mid = 0
    left = 0
    right = 0
    pos = -1

    i = 1
    while i < n:
        element = collection[i].lvl
        toInsert = collection[i]
        sortedListSize = len(sortedList)

        pos = -1
        mid = 0
        left = 0
        right = sortedListSize - 1

        while left < right:
            if pos != -1:
                break
            
            if (right-1) == left:
                if element < sortedList[left].lvl and element < sortedList[right].lvl:
                    pos = left
                    continue
                elif element > sortedList[left].lvl and element > sortedList[right].lvl:
                    pos = right + 1
                    continue
                else:
                    pos = right
                    continue
            else:
                mid = (left + right) // 2

            if element == sortedList[mid].lvl:
                pos = mid
                continue
            elif element < sortedList[mid].lvl:
                right = mid - 1
            else:
                left = mid + 1
        
        if pos == -1:
            if element < sortedList[left].lvl:
                pos = left
            else:
                pos = left + 1
        
        sortedList.insert(pos, toInsert)
        i = i + 1
        
    return sortedList

def padrinoSortType(collection):
    n = len(collection)
    sortedList = []
    sortedListSize = 0
    element = 0

    if n == 0:
        return []
    
    sortedList.append(collection[0])

    mid = 0
    left = 0
    right = 0
    pos = -1

    i = 1
    while i < n:
        element = collection[i].type
        toInsert = collection[i]
        sortedListSize = len(sortedList)

        pos = -1
        mid = 0
        left = 0
        right = sortedListSize - 1

        while left < right:
            if pos != -1:
                break
            
            if (right-1) == left:
                if element < sortedList[left].type and element < sortedList[right].type:
                    pos = left
                    continue
                elif element > sortedList[left].type and element > sortedList[right].type:
                    pos = right + 1
                    continue
                else:
                    pos = right
                    continue
            else:
                mid = (left + right) // 2

            if element == sortedList[mid].type:
                pos = mid
                continue
            elif element < sortedList[mid].type:
                right = mid - 1
            else:
                left = mid + 1
        
        if pos == -1:
            if element < sortedList[left].type:
                pos = left
            else:
                pos = left + 1
        
        sortedList.insert(pos, toInsert)
        i = i + 1
        
    return sortedList

if __name__ == "__main__":
    struct = []

    for _ in range(random.randint(3,90)):
        c = random.randint(1,3)
        l = random.randint(1,10)
        if c == 1:
            t = Village.coinProducer()
        elif c == 2:
            t = Village.shelter()
        elif c == 3:
            t = Village.foodProducer()
        t.setLvl(l)
        struct.append(t)

    print("Unordered")
    for i in range(len(struct)):
        print(struct[i])
    
    struct = padrinoSortShow(struct)

    print("Sorted")
    for i in range(len(struct)):
        print(struct[i])