import Files

class Edge:
    u = 0
    v = 0
    def __init__(self, u, v):
        self.u = u
        self.v = v
    
    def __str__(self):
        return "u: " + str(self.u) + ", v: " + str(self.v)

class Graph:
    nodesNum = 0
    edges = []
    usersIds = []

    def __init__(self):
        self.usersIds = Files.readUsernamesList()

        self.nodesNum = len(self.usersIds) 
        self.edges = []
        for _ in range(self.nodesNum):
            self.edges.append([])

        edges = Files.readEdges()
        for edge in edges:
            self.edges[edge.u].append(edge.v)

    def insertEdge(self, u, v):
        if v != u:
            self.edges[u].append(v)
            self.edges[v].append(u)
            return True
        else:
            return False
    
    def addFriendship(self, uName1, uName2):
        id1 = 0
        id2 = 0

        for i in range(len(self.usersIds)):
            if self.usersIds[i][0] == uName1:
                id1 = i
            elif self.usersIds[i][0] == uName2:
                id2 = i
        
        self.insertEdge(id1, id2)
        return True

    def removeFriendship(self, uName1, uName2):
        id1 = 0
        id2 = 0

        for i in range(len(self.usersIds)):
            if self.usersIds[i][0] == uName1:
                id1 = i
            elif self.usersIds[i][0] == uName2:
                id2 = i
        
        self.edges[id1].remove(id2)
        self.edges[id2].remove(id1)
        return True

    def __str__(self):
        r = ""
        for i in range(self.nodesNum):
            r += str(i) + " : "
            for j in range(len(self.edges[i])):
                r += str(self.edges[i][j]) + " "
            r += "\n"
        r = r[:-1]
        return r
    
    def save(self):
        edgesList = []
        for i in range(self.nodesNum):
            for j in range(len(self.edges[i])):
                edgesList.append([i , self.edges[i][j]])
        Files.writeEdges(edgesList)

    def bfsFriends(self, username):
        id = 0
        for i in range(len(self.usersIds)):
            if self.usersIds[i][0] == username:
                id = i
                break

        near = []
        visited = [0] * self.nodesNum
        depth = [0] * self.nodesNum

        queue = []
        queue.append(id)
        visited[id] = 1
        depth[id] = 0

        pt = 0
        while queue:
            pt = queue.pop(0)
            visited[pt] = 1
            for i in range(len(self.edges[pt])):
                toNode = self.edges[pt][i]
                if visited[toNode] == 0:
                    visited[toNode] = 1
                    depth[toNode] = depth[pt] + 1
                    if depth[toNode] <= 1:
                        queue.append(toNode)
                        near.append(toNode)
        res = []
        for i in range(len(self.usersIds)):
            if i in near:
                res.append(self.usersIds[i][0])

        return res

    def bfsEnemies(self, username):
        id = 0
        for i in range(len(self.usersIds)):
            if self.usersIds[i][0] == username:
                id = i
                break

        near = []
        visited = [0] * self.nodesNum
        depth = [0] * self.nodesNum

        queue = []
        queue.append(id)
        visited[id] = 1
        depth[id] = 0
        near.append(id)

        pt = 0
        while queue:
            pt = queue.pop(0)
            visited[pt] = 1
            for i in range(len(self.edges[pt])):
                toNode = self.edges[pt][i]
                if visited[toNode] == 0:
                    visited[toNode] = 1
                    depth[toNode] = depth[pt] + 1
                    if depth[toNode] <= 2:
                        queue.append(toNode)
                        near.append(toNode)
        res = []
        for i in range(len(self.usersIds)):
            if i not in near:
                res.append(self.usersIds[i][0])

        return res


if __name__ == "__main__":
    g = Graph()
    print(g)
    g.addFriendship("gus","zzzlano")
    print()
    print(g)
    g.removeFriendship("gus","zzzlano")
    print()
    print(g)