import Files
import Graph
import Search
import Sort
import Tree
import User
import Village
import random

activeUser = None
logged = False
userTree = Files.readUsers()
completeLore = Files.readAllStructures()

#1 - log in
    #Interact village
        #Village information
            #Print structures
            #Print lore
        #Buy structure
        #Upgrade structure
        #Sell structure (optional)
        #Search structure
        #Return

    #Social
        #Add friend
        #Delete friend
        #Search friend
        #Show friends
        #Return

    #Attack (random)
        #Attack
        #Next
        #Exit

    #Log out

#2 - Create account
#3 - Exit

def villageMenu():
    global activeUser

    while True:
        print("1 - Village information")
        print("2 - Buy structure")
        print("3 - Upgrade structure")
        print("4 - Sell structure")
        print("5 - Search structure")
        print("6 - Return")


        opt = input("Choose an option: ") 
        if opt.isdigit():
            opt = int(opt)
        else:
            print("Enter a valid digit [1-6]")
            continue
        
        activeUser.village.produceCoin()
        Files.writeVillage(activeUser.username, activeUser.village)

        if opt == 1:
            #Village information
            while True:
                print("1 - Show structures")
                print("2 - Sort structures")
                print("3 - Show lore")
                print("4 - Return")

                opt1 = input("Choose an option: ") 
                if opt1.isdigit():
                    opt1 = int(opt1)
                else:
                    print("Enter a valid digit [1-3]")
                    continue
                
                if opt1 == 1:
                    print(str(activeUser.village))
                elif opt1 == 2:
                    print("1 - By name")
                    print("2 - By level")
                    print("3 - By type")
                    print("4 - By sections")
                    opt2 = input("Choose an option: ")
                    while opt2.isdigit() == False or int(opt2) > 4 or int(opt2) < 1:
                        print ("Enter a valid digit ")
                        opt2 = input()
                    opt2 = int(opt2)
                    
                    if opt2 == 1:
                        activeUser.village.structs = Sort.padrinoSortShow(activeUser.village.structs)
                        print(str(activeUser.village))
                    elif opt2 == 2:
                        activeUser.village.structs = Sort.padrinoSortLevel(activeUser.village.structs)
                        print(str(activeUser.village))
                    elif opt2 == 3:
                        activeUser.village.structs = Sort.padrinoSortType(activeUser.village.structs)
                        print(str(activeUser.village))
                    elif opt2 == 4:
                        activeUser.village.structs = Sort.padrinoSortType(activeUser.village.structs)
                        type1List = activeUser.village.structs[:activeUser.village.coinProducerNum]
                        type2List = activeUser.village.structs[activeUser.village.coinProducerNum:(activeUser.village.coinProducerNum + activeUser.village.shelterNum)]
                        type3List = activeUser.village.structs[(activeUser.village.coinProducerNum + activeUser.village.shelterNum):]                        
                        type1List = Sort.padrinoSortLevel(type1List)
                        type2List = Sort.padrinoSortLevel(type2List)
                        type3List = Sort.padrinoSortLevel(type3List)
                        activeUser.village.structs = []
                        activeUser.village.structs = type1List + type2List + type3List
                        print(str(activeUser.village))
                elif opt1 == 3:
                    lore = Files.lore2User()
                    print(lore)
                elif opt1 == 4:
                    break
                else:
                    print("Enter a valid digit [1-3]")
        elif opt == 2:
            #Buy structure
            if len(activeUser.village.structs) < 99 and activeUser.village.coins > 10:
                print("Enter the structure type you want to build")
                print("[1] - Coin generator")
                print("[2] - Shelter")
                print("[3] - Food generator")

                opt1 = input()
                while opt1.isdigit() == False or int(opt1) > 3 or int(opt1) < 1:
                    print ("Not a valid option")
                    opt1 = input()
                opt1 = int(opt1)

                activeUser.village.addStructure(opt1)
                activeUser.village.coins -= 10
                if opt1 == 1:
                    print("Coin generation structure added")
                elif opt1 == 2:
                    print("Shelter structure added")
                elif opt1 == 3:
                    print("Food generation structure added")
                Files.writeVillage(activeUser.username, activeUser.village)
            else:
                if len(activeUser.village.structs) >= 99:
                    print("Village already full, structure can not be added")
                if activeUser.village.coins < 10:
                    print("Not enough coins to build, you need at least 10 coins")
        elif opt == 3:
            #Upgrade structure
            print("Search the structure to upgrade by:")
            print("1 - Name")
            print("2 - Level and type")
            opt1 = input()
            while opt1.isdigit() == False or int(opt1) > 2 or int(opt1) < 1:
                print ("Not a valid option")
                opt1 = input()
            opt1 = int(opt1)

            if opt1 == 1:
                opt1 = input("Type the name of the structure to upgrade: ")
                piv = Search.linearSearchName(completeLore, opt1)
                if piv != []:
                    if activeUser.village.coins >= ((piv[0].lvl+1) * 10):
                        b = activeUser.village.upgradeStructure(piv[0].type, piv[0].lvl)
                        if(b == True):
                            print("Structure upgraded")
                            activeUser.village.coins -= ((piv[0].lvl+1)* 10)
                        else:
                            print("Structure can not be upgraded")
                    else:
                        print("Not enough coins, you need at least " + str( (piv[0].lvl+1) * 10) + " coins")
                else:
                    print("Strucutre does not exist.")
            elif opt1 == 2:
                opt1 = input("Enter the structure level you want to upgrade: [1-9] ")
                while opt1.isdigit() == False or int(opt1) > 9 or int(opt1) < 1:
                    if opt1 == 10:
                        print("Level 10 is the max level for all the structures, you can not upgrade")
                    else:
                        print ("Not a valid level")
                    opt1 = input()
                opt1 = int(opt1)
                if activeUser.village.coins >= (opt1 * 10):
                    opt2 = input("Now enter the type: ")
                    while opt2.isdigit() == False or int(opt2) > 3 or int(opt2) < 1:
                        print("Not a valid type [1 - 3]")
                        opt2 = input()
                    opt2 = int(opt2)

                    if activeUser.village.searchStructure(opt2, opt1) == None:
                        print("Structure not found")
                    else:
                        b = activeUser.village.upgradeStructure(opt2, opt1)
                        if(b == True):
                            print("Structure upgraded")
                            activeUser.village.coins -= ((opt1+1)* 10)
                        else:
                            print("Structure can not be upgraded")
                else:
                    print("Not enough coins, you need at least " + str(opt1 * 10) + " coins")

        elif opt == 4:
            #Sell structure
            print("Search the structure to delete by:")
            print("1 - Name")
            print("2 - Level and type")

            opt1 = input()
            while opt1.isdigit() == False or int(opt1) > 2 or int(opt1) < 1:
                print ("Not a valid option")
                opt1 = input()
            opt1 = int(opt1)

            if opt1 == 1:
                opt1 = input("Type the name of the structure to delete: ")
                piv = Search.linearSearchName(completeLore, opt1)
                if piv != []:
                    b = activeUser.village.delStructure(piv[0].type, piv[0].lvl)
                    if b == True:
                        print("Structure sold")
                        activeUser.village.coins += 1
                    else:
                        print("Structure can not sold")
                else:
                    print("Structure not found")
            elif opt1 == 2:
                opt1 = input("Enter the structure level you want to sell: [1 - 10]")
                while opt1.isdigit() == False or int(opt1) > 10 or int(opt1) < 1:
                    print ("Not a valid level [1 - 10]")
                    opt1 = input()
                opt1 = int(opt1)
                opt2 = input("Now enter the type: ")
                print("[1] - Coin generator")
                print("[2] - Shelter")
                print("[3] - Food generator")
                while opt2.isdigit() == False or int(opt2) > 3 or int(opt2) < 1:
                    print("Not a valid type [1 - 3]")
                    opt2 = input()
                opt2 = int(opt2)

                b = activeUser.village.delStructure(opt2, opt1)
                if(b == True):
                    print("Structure sold")
                    activeUser.village.coins += 1
                else:
                    print("Structure can not be sold")

        elif opt == 5:
            #Search structure
            print("Search structure by:")
            print("1 - Name")
            print("2 - Level")
            print("3 - Type")
            opt1 = input()
            while opt1.isdigit() == False or int(opt1) > 3 or int(opt1) < 1:
                print ("Not a valid option")
                opt1 = input()
            opt1 = int(opt1)

            if opt1 == 1:
                opt2 = input("Type the name of the structure to search: [1 - 3]")
                piv = Search.linearSearchName(completeLore, opt2)
                if piv != []:
                    lvl1 = piv[0].lvl
                    type1 = piv[0].type
                    piv = Search.linearSearchLevel(activeUser.village.structs, lvl1)
                    piv = Search.linearSearchType(piv, type1)
                    if(piv != []):
                        print("\"" + opt2 + "\"" + " structures:")
                        for i in range(len(piv)):
                            print(str(i + 1) + ". Name: " + Village.getStructureName(piv[i].type, piv[i].lvl)  + ", Level: " + str(piv[i].lvl) + ", Type: " + str(piv[i].type))
                    else:
                        print("There is no structure with that name")
                else:
                    print("Strucutre does not exist.")
            elif opt1 == 2:
                opt2 = input("Enter the structure level you want to search: [1-10]")
                while opt2.isdigit() == False or int(opt2) > 10 or int(opt2) < 1:
                    if opt2 == 10:
                        print("Level 10 is the max level for all the structures, you can not upgrade")
                    else:
                        print ("Not a valid level")
                    opt2 = input()
                opt2 = int(opt2)

                piv = Search.linearSearchLevel(activeUser.village.structs, opt2)
                if(piv != []):
                    lvlList = []
                    lvlList.append(completeLore[opt2 - 1])
                    lvlList.append(completeLore[opt2 + 9])
                    lvlList.append(completeLore[opt2 + 19])
                    print("Level " + str(opt2) + " structures:")
                    for i in range(len(piv)):
                        print(str(i) + ". Name: " + lvlList[piv[i].type - 1].name + ", Type: " + str(piv[i].type))
                else:
                    print("There is no structure with that level")
            elif opt1 == 3:
                print("[1] - Coin generator")
                print("[2] - Shelter")
                print("[3] - Food generator")
                opt2 = input("Enter the structure type you want to search: ")
                while opt2.isdigit() == False or int(opt2) > 3 or int(opt2) < 1:
                    print("Not a valid type")
                    opt2 = input()
                opt2 = int(opt2)
                
                typeList = []
                if opt2 == 1:
                    typeList = completeLore[:10]
                elif opt2 == 2:
                    typeList = completeLore[10:20]
                elif opt2 == 3:
                    typeList = completeLore[20:]                    

                piv = Search.linearSearchType(activeUser.village.structs, opt2)
                if(piv != []):
                    print("Type " + str(opt2) + " structures:")
                    for i in range(len(piv)):
                        print(str(i) + ". Name: " + typeList[piv[i].lvl - 1].name + ", Level: " + str(piv[i].lvl))
                else:
                    print("There is no structures with that type")
        elif opt == 6:
            return
        else:
            print("Enter a valid digit [1-6]")

def socialMenu():
    global activeUser
    global userTree

    relationGraph = Graph.Graph()

    while True:
        print("1 - Show friends")
        print("2 - Search user")
        print("3 - Add friend")
        print("4 - Delete friend")
        print("5 - Show attacked history")
        print("6 - Show defended history")
        print("7 - Return")

        opt = input("Choose an option: ") 
        if opt.isdigit():
            opt = int(opt)
        else:
            print("Enter a valid digit [1-5]")
            continue
        
        activeUser.village.produceCoin()

        if opt == 1:
            friendList = relationGraph.bfsFriends(activeUser.username)
            friendList = Sort.padrinoSortUserName(friendList)
            print("Friends: ")
            for f in friendList:
                print("Username: " + f)
        elif opt == 2:
            searched = userTree.search(input("Username to search: ")) 
            if searched != None:
                print("Found!")
                v = Files.readVillage(searched.username)
                print(searched)
                print(v)
            else:
                print("User does not exist")
            print()
        elif opt == 3:
            searched = userTree.search(input("Username to search: ")) 
            if searched != None:
                print("Found!")
                relationGraph.addFriendship(activeUser.username, searched.username)
                relationGraph.save()
            else:
                print("User does not exist")
        elif opt == 4:
            searched = userTree.search(input("Username to search: ")) 
            if searched != None:
                if searched.username in relationGraph.bfsFriends(activeUser.username):
                    relationGraph.removeFriendship(activeUser.username, searched.username)
                    print("Friendship ended")
                    relationGraph.save()
                else:
                    print("User is not your friend")
            else:
                print("User does not exist")
        elif opt == 5:
            history = Files.readFightHistory(activeUser.username)
            for i in range(len(history)):
                if history[i][0] == activeUser.username:
                    print("Attacked: " + history[i][1] + ", " + str(history[i][2]) + " coins stolen.")
        elif opt == 6:
            history = Files.readFightHistory(activeUser.username)
            for i in range(len(history)):
                if history[i][1] == activeUser.username:
                    print("Defended: " + history[i][1] + ", loosed " + str(history[i][2]) + " coins.")
        elif opt == 7:
            return
        else:
            print("Not a valid option")
 
def attackMenu():
    global activeUser
    global userTree

    relationGraph = Graph.Graph()
    i = 0

    enemies = relationGraph.bfsEnemies(activeUser.username)
    if not enemies:
        print("You cant attack anybody, you dont have enemies")
        return

    random.shuffle(enemies)

    while True:
        attackedVillage = Files.readVillage(enemies[i])
        print("Username: " + enemies[i])
        attackedVillage.reducedPrint()

        print("1 - Attack")
        print("2 - Next")
        print("3 - Return")

        opt = input("Choose an option: ") 
        if opt.isdigit():
            opt = int(opt)
        else:
            print("Enter a valid digit [1-3]")
            continue

        if opt == 1:
            if activeUser.village.lvl > attackedVillage.lvl:
                prob = random.randint(4,10)
            elif activeUser.village.lvl < attackedVillage.lvl:
                prob = random.randint(0,6)
            else:
                prob = random.randint(0,10)
            
            if prob > 5:
                attackedCoins = attackedVillage.coins
                if not attackedVillage.bot:
                    attackedVillage.coins -= attackedCoins//4
                    Files.writeVillage(enemies[i], attackedVillage)
                activeUser.village.coins += attackedCoins//4
                Files.writeVillage(activeUser.username, activeUser.village)
                Files.writeFightHistory(activeUser.username, enemies[i], attackedCoins//4)
                print("You won, received " + str(attackedCoins//4) + " coins.")
            else:
                userCoins = activeUser.village.coins
                activeUser.village.coins -= userCoins//4
                Files.writeFightHistory(activeUser.username, enemies[i], -(userCoins//4))
                Files.writeVillage(activeUser.username, activeUser.village)
                print("You lost, lost " + str(userCoins//4) + " coins.")

            activeUser.village.produceCoin()
        elif opt == 2:
            i += 1
            if i >= len(enemies):
                random.shuffle(enemies)
                i = 0
        elif opt == 3:
            return
        else:
            print("Not a valid option")   

def secondaryMenu():
    global activeUser
    while True:
        print("1 - Interact with Village")
        print("2 - Social")
        print("3 - Attack")
        print("4 - Log out")

        opt = input("Choose an option: ") 
        if opt.isdigit():
            opt = int(opt)
        else:
            print("Enter a valid digit [1-4]")
            continue

        if opt == 1:
            villageMenu()
        elif opt == 2:
            socialMenu()
        elif opt == 3:
            attackMenu()
        elif opt == 4:
            return
        else:
            print("Not a valid option")
        
def main():
    global activeUser
    global logged
    global userTree

    print("Welcome to Squad-Revolt")
    while True:
        print("Menu options: ")
        print("1 - Log in")
        print("2 - Create account")
        print("3 - Exit")

        opt = input("~: ")

        # Chech if valid input
        if opt.isdigit():
            opt = int(opt)
        else:
            print("Enter a valid digit [1-3]")
            continue

        if opt == 1:
            activeUser = User.userLoginIT(userTree)
            if activeUser != None:
                print("Succesfull login!")
                activeUser.village = Files.readVillage(activeUser.username)
                logged = True
                secondaryMenu()
                logged = False
                Files.writeVillage(activeUser.username, activeUser.village)
                activeUser = None
        elif opt == 2:
            activeUser = User.registerUserIT(userTree)
            if activeUser != None:
                print("Sucessfull register, please login")
                activeUser = None
        elif opt == 3:
            print("Thank you for playing!")
            return
        else:
            print("Not a valid option")

if __name__ == "__main__":
    main()